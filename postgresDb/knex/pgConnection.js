const knex = require('knex');

const pgConnection = knex({
    client: 'pg',
    connection: {
        host : '127.0.0.1',
        port : 5433,
        user : 'postgres',
        password : 'admin',
        database: 'postgres'
    },
    pool: {
        min: 0,
        max: 10
    }
});

module.exports = pgConnection;
