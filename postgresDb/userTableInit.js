const pgConnection = require('./knex/pgConnection');

const createUserTable = async () => {
    await pgConnection.schema.createTable('users', function (table) {
        table.increments();
        table.string('name');
        table.integer('age');
        table.timestamps();
    });
};

const insertUsers = async () => {
    const inserted = await pgConnection.insert([
        {name: 'Denis', age: 30},
        {name: 'Alice', age: 25},
        {name: 'Victoria', age: 24},
        {name: 'Radelia', age: 29}
    ]).into('users');
};


createUserTable().then(() => {
        insertUsers().then((inserted) => {
            console.log('inserted', inserted);
        });
});
