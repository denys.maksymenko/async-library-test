const Tests = require('./tests');

const test = process.argv[2];

switch (test) {
    case 'waterfall': {
        Tests.waterfallTest(); break;
    }
    case 'series': {
        Tests.seriesTest(); break;
    }
    case 'parallel': {
        Tests.parallelTest(); break;
    };
    case 'each': {
        Tests.eachTest(); break;
    };
    case 'until': {
        Tests.untilTest(); break;
    };
    case 'mongoDb': {
        Tests.mongoDb().then((res) =>{
            console.log('Result', res);
            console.log('Script finished')
        }); break;
    }
    case 'postgresDb': {
        Tests.postgresDb().then((res) =>{
            console.log('Result', res);
            console.log('Script finished')
        }); break;
    }
};
