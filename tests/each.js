const async = require('async');

// void функция которая пробегает по массиву и для каждого значения массива выполняет ассинхронную callback void функцию (iterator)
// третьим параметром передается callback функция которая выполняется после завершения each или в случае ошибки
// iterator - ничего не возвращает, даже если ему передать вторым аргументом результат 

const eachTest = function() {
    console.time('timer');
    const obj = {}
    const asyncRes = async.each([1,2,3,4,5], (number, cb) => {
        const cube = number ** 3;
        obj[number] = cube;
        return cb(null, number ** 2);
    }, (err, res) => {
        if(err) {
            console.log(err.message);
            console.timeEnd('timer');
            return;
        }
        console.log('res', res); // undefined
        console.log('EachTest complete'); // show if no error
    });
    console.log('obj', obj); // obj { '1': 1, '2': 8, '3': 27, '4': 64, '5': 125 }
    console.log('asyncRes', asyncRes); // undefined
    console.timeEnd('timer');
};


module.exports = eachTest;
