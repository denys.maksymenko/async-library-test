const async = require('async');

// в отличие от waterfall, series не передает результаты от предыдущей функции, а записывает их в результирующий массив. Ближайший аналог promise.All. 
// Но функции выполняется последовательно одна за другой. При этом сигнатура функций содержит толко callback, иначе ошибка.
// привязки к наименованию cb нет. Это может быть и callback, и next, и сat

const seriesTest = function() {
    console.time('timer');
    async.series([
       (callback) => {
           setTimeout(() => {
            return callback(null, 10)
           }, 5000);
       },
       (next) => {
            setTimeout(() => {
                return next(null, 7)
            }, 3000);
       },
       (cat) => {
            setTimeout(() => {
                return cat(null, 30);
            }, 2000);
       }
   ], (err, result) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log('Result', result);
        console.timeEnd('timer');
    });
};

// Result [ 10, 7, 30 ]
// timer: 10026.063ms

module.exports = seriesTest;
