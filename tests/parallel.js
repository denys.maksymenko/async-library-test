const async = require('async');

// то же что и series  но функции выполнются не дожидаясь предыдущей, паралельно. Тем не менее результат будет получен после отрабатывания всех
// функций и в том же порядке, что и в массиве.

const parallelTest = function() {
    console.time('timer');
    async.parallel([
       (callback) => {
           setTimeout(() => {
            return callback(null, 11)
           }, 5000);
       },
       (next) => {
            setTimeout(() => {
                return next(null, 8)
            }, 3000);
       },
       (cat) => {
            setTimeout(() => {
                return cat(null, "Cat");
            }, 2000);
       }
   ], (err, result) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log('Result', result);
        console.timeEnd('timer');
    });
};

// Result [ 11, 8, 'Cat' ]
// timer: 5015.687ms

module.exports = parallelTest;
