const async = require('async');

// выполняет второй-аргумент-функцию до тех пор пока первый-аргумент-функция не станет true
// возможен бесконечный цикл

const untilTest = function() {
    console.time('timer');
    let count = 1;
    async.until(() => count == 10, (cb) => {
        console.log('count', count);
        count++;
        return cb(null, count);
    }, (err, res) => {
        if(err) {
            console.log(err.message);
            console.timeEnd('timer');
            return;
        }
        console.log('res', res); // undefined
        console.log('EachTest complete'); // show if no error
    });
    console.timeEnd('timer');
};


module.exports = untilTest;
