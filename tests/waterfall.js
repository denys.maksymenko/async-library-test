const async = require('async');

// test async.waterfall
// фактически конвеер, выполняет последовательно функции указанные в массиве, результат выполнения передает в следующую функцию.
// при этом сигнатура выполняемых функций function(argument, cb). Если в cb попадает результат cb(null, res) то этот результат передается 
// в качестве argument в следующую функцию.При этом текущая функция не завершается. Если ошибка, cb(err) то текущая функция не завершается,а 
// выполнение всех последующих функций завершается и вызывается главный callback с ошибкой err, callback(err).

// в качестве теста перемножим три числа, 9,7, 2.
const waterfallTest = function() {
    console.time('timer');
    async.waterfall([
        (cb) => {
            setTimeout(() => {
                return cb(null, 9);
            }, 5000)
        },
        (firstNumber, cb) => {
            setTimeout(() => {
                return cb(null, firstNumber * 7);
            }, 3000)
        },
        (secondNumber, cb) => {
            return cb(null, secondNumber * 2);
        }
    ], (err, result)=> {
        if(err) {
            console.log(err);
            return;
        }
        console.log('Result', result);
        console.timeEnd('timer');
    });
}

// Result 126
// timer: 8019.560ms

module.exports = waterfallTest;
