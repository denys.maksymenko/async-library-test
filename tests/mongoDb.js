const mongoose = require('mongoose');
const UserModel = require('../mongoDb/schems/User');

const run = async () => {
    await mongoose.connect('mongodb://root:admin@localhost:27018/admin');
    const res= await UserModel.aggregate([
        {
            $project: {age: { $divide: [ "$age", 2 ] }}
        }
    ]);
    return res;
}

module.exports = run;
