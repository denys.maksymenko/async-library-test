const waterfallTest = require('./waterfall');
const seriesTest = require('./series');
const parallelTest = require('./parallel');
const eachTest = require('./each');
const untilTest = require('./until');
const mongoDb = require('./mongoDb');
const postgresDb = require('./postgresDb');

module.exports = {
    waterfallTest,
    seriesTest,
    parallelTest,
    eachTest,
    untilTest,
    mongoDb,
    postgresDb
}